/**
 * Backend Server startup file, handles incoming api's and mongo connection
 * @author - Abhishek Dixit <dixit2794@gmail.com>
 */
"use strict";
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
var firebase = require("firebase");

const port = process.env.PORT || 4000;

/**
 * Setting Up the headers for incoming requests
 */
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});
/**
 * Setting Up the middleware
 */
app.use(bodyParser.json());

/**
 * Setting Up the routers
 */
const userRouter = require("./Routes/userRoutes.js")();
app.use("/user", userRouter);

/**
 * Server Listening
 */
app.listen(port, function () {
  firebase.initializeApp({
    apiKey: "AIzaSyDrIK51_A_Q21-lwtv7j-r3S7XenW6oY_U",
    authDomain: "sprinkler-7ba54.firebaseapp.com",
    databaseURL: "https://sprinkler-7ba54.firebaseio.com",
    projectId: "sprinkler-7ba54",
    storageBucket: "sprinkler-7ba54.appspot.com",
    messagingSenderId: "744621462874",
    appId: "1:744621462874:web:33d4ceab79b7f0bce68f63",
  });
  console.log("Backend Server is Running on http://localhost:" + port);
});
