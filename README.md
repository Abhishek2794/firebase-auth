# Backend

Backend server to handle the API requests written in NodeJS

### Server Deployment Steps

- Install NodeJs (Linux CMD to install NodeJS: sudo apt install nodejs)
- Install PM2 to run nodejs server (npm i pm2 -g)
- Clone the repo on the server
- In the root of repo run this CMD: (npm i)
- In the root of repo run this CMD: (pm2 start app.js)

### `Install and Run`

- Install required package using `npm install` in project directory
- Run using `node app.js` or `npm start`

### Get User Token

- POST Api - localhost:4000/user/authenticate
- Body Params - email, password
