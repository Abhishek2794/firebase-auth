"use strict";

const express = require("express");
const msg = require("../Config/messageConfig");
var firebase = require("firebase");

var routes = function () {
  var userRouter = express.Router();

  userRouter.route("/authenticate").post(function (req, res) {
    const body = req.body;
    if (!body.email || !body.password) {
      res.send({
        ...msg.invalidParams,
        response: "email and password are required",
      });
    } else {
      firebase
        .auth()
        .signInWithEmailAndPassword(body.email, body.password)
        .then(async (response) => {
          const authToken = await firebase.auth().currentUser.getIdToken();
          res.status(200).send(authToken);
        })
        .catch((error) => {
          const errorMessage = error.message;
          res.send({
            status: 400,
            message: errorMessage,
          });
        });
    }
  });

  return userRouter;
};

module.exports = routes;
